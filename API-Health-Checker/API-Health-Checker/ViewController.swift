//
//  ViewController.swift
//  API-Health-Checker
//
//  Created by Blaine Rothrock on 11/13/15.
//  Copyright © 2015 blainerothrock. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    //outlets
    @IBOutlet weak var endpointTextField: UITextField!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func submitTouched(sender: UIButton) {
        
        let endpoint = endpointTextField.text
    
        
        
        Alamofire.request(.GET, endpoint!,parameters: nil).responseJSON { (response) -> Void in
            
            let json = JSON(data: response.data!)
            
            self.outputTextView.text = json.rawString()
            print("result: \(json)")
        }
    }
}

